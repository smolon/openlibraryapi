package com.example.openlibraryapi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final int DETAILS_BOOK_ACTIVITY_REQUEST_CODE = 1;
    private Book detailBook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.book_menu, menu);
        MenuItem clearSearchItem = menu.findItem(R.id.menu_item_clear);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        clearSearchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                fetchBooksData("");
                return true;
            }
        });
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchBooksData(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    private void fetchBooksData(String query){
        String finalQuery = prepareQuery(query);
        BookService bookService = RetrofitInstance.getRetrofitInstance().create(BookService.class);
        Call<BookContainer> booksApiCall = bookService.findBooks(finalQuery);
        booksApiCall.enqueue(new Callback<BookContainer>() {
            @Override
            public void onResponse(Call<BookContainer> call, Response<BookContainer> response) {
                setupBookListView(response.body().getBookList());
                if(response.body().getBookList().isEmpty()){
                    Snackbar.make(findViewById(R.id.recyclerview), "Brak danych", Snackbar.LENGTH_LONG).show();
                    Log.d("POwiedz", "poweiedx czemu");
                }
            }

            @Override
            public void onFailure(Call<BookContainer> call, Throwable t) {
                Snackbar.make(findViewById(R.id.recyclerview), "Something went wrong... Please try later!", Snackbar.LENGTH_LONG).show();
            }
        });
    }
    private String prepareQuery(String query) {
        String[] queryParts = query.split("\\s+");
        return TextUtils.join("+", queryParts);
    }
    private void setupBookListView(List<Book> books){
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final BookAdapter adapter = new BookAdapter();
        adapter.setBooks(books);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    public boolean checkNullOrEmpty(String text){
        return text != null && !TextUtils.isEmpty(text);
    }
    private class BookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String IMAGE_URL_BASE = "http://covers.openlibrary.org/b/id/";
        private TextView bookTitleTextView;
        private TextView bookAuthorTextView;
        private ImageView bookCover;
        private String imgPath;
        private Book book;
        public BookHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.book_list_item, parent, false));
            itemView.setOnClickListener(this::onClick);
            bookTitleTextView = itemView.findViewById(R.id.book_title);
            bookAuthorTextView = itemView.findViewById(R.id.book_author);
            bookCover = itemView.findViewById(R.id.img_cover);
        }

        @Override
        public void onClick(View v) {
            detailBook = book;
            Intent detailsIntent = new Intent(MainActivity.this, BookDetails.class);
            detailsIntent.putExtra(BookDetails.EXTRA_DETAILS_BOOK_TITLE, bookTitleTextView.getText().toString());
            detailsIntent.putExtra(BookDetails.EXTRA_DETAILS_BOOK_AUTHOR, bookAuthorTextView.getText().toString());
            detailsIntent.putExtra(BookDetails.EXTRA_DETAILS_BOOK_COVER, detailBook.getCover());
            detailsIntent.putExtra(BookDetails.EXTRA_DETAILS_BOOK_BOOK, detailBook);
            startActivity(detailsIntent);
        }

        public void bind(Book book){
            this.book = book;
            if(book != null && checkNullOrEmpty(book.getTitle()) && book.getAuthors() != null){
                bookTitleTextView.setText(book.getTitle());
                bookAuthorTextView.setText(TextUtils.join(", ", book.getAuthors()));
                if(book.getCover() != null){
                    Picasso.with(itemView.getContext())
                            .load(IMAGE_URL_BASE + book.getCover() + "-M.jpg")
                            .placeholder(R.drawable.ic_book_black_24dp).into(bookCover);
                } else {
                    bookCover.setImageResource(R.drawable.ic_book_black_24dp);
                }
            }
        }
    }
    private class BookAdapter extends RecyclerView.Adapter<BookHolder> {

        private List<Book> books;

        @NonNull
        @Override
        public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BookHolder(getLayoutInflater(), parent);
        }

        @Override
        public void onBindViewHolder(@NonNull BookHolder holder, int position) {
            if(books != null) {
                Book book = books.get(position);
                holder.bind(book);
            }
            else
                Log.d("MainActivity", "No Books");
        }

        @Override
        public int getItemCount() {
            if(books != null)
                return books.size();
            return 0;
        }

        public void setBooks(List<Book> books) {
            this.books = books;
            notifyDataSetChanged();
        }
    }
}
