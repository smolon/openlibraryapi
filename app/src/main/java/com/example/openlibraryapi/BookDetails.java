package com.example.openlibraryapi;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class BookDetails extends AppCompatActivity {
    public static final String EXTRA_DETAILS_BOOK_TITLE = "tytul";
    public static final String EXTRA_DETAILS_BOOK_AUTHOR = "author";
    public static final String EXTRA_DETAILS_BOOK_COVER = "cover";
    public static final String EXTRA_DETAILS_BOOK_BOOK = "book";
    private static final String IMAGE_URL_BASE = "http://covers.openlibrary.org/b/id/";
    private TextView titleDetailText;
    private TextView authorDetailText;
    private TextView publishDate;
    private ImageView coverImage;
    private Book book;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_details);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        titleDetailText = findViewById(R.id.details_book_title);
        authorDetailText = findViewById(R.id.details_book_author);
        publishDate = findViewById(R.id.details_book_publishDate);
        titleDetailText.setText(getString(R.string.title, getIntent().getStringExtra(EXTRA_DETAILS_BOOK_TITLE)));
        authorDetailText.setText(getString(R.string.author, getIntent().getStringExtra(EXTRA_DETAILS_BOOK_AUTHOR)));
        book =  (Book) getIntent().getSerializableExtra(EXTRA_DETAILS_BOOK_BOOK);
        coverImage = findViewById(R.id.img_cover);
        publishDate.setText(getString(R.string.first_publish_date, book.getPublishYear()));
        Picasso.with(coverImage.getContext()).load(IMAGE_URL_BASE + getIntent().getStringExtra(EXTRA_DETAILS_BOOK_COVER) + "-L.jpg").into(coverImage);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
